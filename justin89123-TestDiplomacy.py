#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from re import L
from unittest import main, TestCase
import sys

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy, diplomacy_print

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_diplomacy_read_1(self):
        s = ["a b c"]
        self.assertEqual(diplomacy_read(s), {'a': {"Location" : "b", "Action" : ["c"], "Support":0}}) 

    def test_diplomacy_read_2(self):
        s = ["a london support b"]
        self.assertEqual(diplomacy_read(s), {'a': {"Location" : "london", "Action": ["support", "b"], "Support":0}}) 
    
    def test_diplomacy_read_3(self):
        s = ["E Austin Support A"]
        self.assertEqual(diplomacy_read(s), {'E': {"Location" : "Austin", "Action": ["Support", "A"], "Support":0}})

    def test_diplomacy_read_4(self):
        s = ["B Barcelona Move Madrid", "E Austin Support A"]
        self.assertEqual(diplomacy_read(s), {'B': {"Location" : "Barcelona", "Action": ["Move", "Madrid"], "Support":0}, 'E': {"Location" : "Austin", "Action": ["Support", "A"], "Support":0}}) 
    

    def test_diplomacy_1(self):
        inp = {"A" : {"Location":"Austin", "Action":["Hold"], "Support":0}, "B":{"Location":"Barcelona", "Action":["Move", "Austin"], "Support":0}}
        self.assertEqual(diplomacy(inp), [["A","[dead]"], ["B", "[dead]"]])
    
    def test_diplomacy_2(self):
        inp = {"A" : {"Location":"Austin", "Action":["Hold"], "Support":0}, "B":{"Location":"Barcelona", "Action":["Move", "Austin"], "Support":0}, "C":{"Location":"Tallahasse", "Action":['Support', "B"], "Support":0}}
        self.assertEqual(diplomacy(inp), [["A","[dead]"], ["B","Austin"], ["C","Tallahasse"]])
    
    def test_diplomacy_3(self):
        inp = diplomacy_read(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"])
        self.assertEqual(diplomacy(inp), [["A","[dead]"], ["B","[dead]"], ["C","[dead]"], ["D", "Paris"], ["E","Austin"]])
    
    def test_diplomacy_4(self):
        inp = diplomacy_read(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Barcelona"])
        self.assertEqual(diplomacy(inp), [["A","[dead]"], ["B","[dead]"], ["C","Barcelona"]])
    
    def test_diplomacy_5(self):
        inp = diplomacy_read(["A Madrid Move Barcelona", "B Barcelona Move Madrid", "C London Move Austin", "D Austin Move London"])
        self.assertEqual(diplomacy(inp), [["A","Barcelona"], ["B","Madrid"], ["C","Austin"], ["D","London"]])

    def test_diplomacy_6(self):
        inp = diplomacy_read(["A Madrid Hold", "B Madrid Hold", "C London Move Austin", "D Austin Move London"])
        self.assertEqual(diplomacy(inp), [["A","[dead]"], ["B","[dead]"], ["C","Austin"], ["D","London"]])
    
    def test_diplomacy_7(self):
        inp = diplomacy_read(["A Madrid Hold", "B China Hold", "C London Support A", "D Austin Move London"])
        self.assertEqual(diplomacy(inp), [["A","Madrid"], ["B","China"], ["C","[dead]"], ["D","[dead]"]])
    
    def test_diplomacy_print_1(self):
        w = StringIO()
        inp = [["A", "London"], ["B", "Austin"], ["C", "France"]]
        diplomacy_print(w, inp)
        self.assertEqual(w.getvalue(), "A London\nB Austin\nC France\n")
    
    def test_diplomacy_print_2(self):
        w = StringIO()
        inp = [["A", "[dead]"], ["B", "Austin"], ["C", "France"]]
        diplomacy_print(w, inp)
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\nC France\n")
    
    def test_diplomacy_solve_1(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Barcelona"]
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")
    
    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")

    

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover

"""
